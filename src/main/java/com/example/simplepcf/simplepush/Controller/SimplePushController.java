package com.example.simplepcf.simplepush.Controller;

import com.example.simplepcf.simplepush.Model.SimplePushModel;
import com.example.simplepcf.simplepush.Repository.SimplePushRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class SimplePushController
{
    // ***** Member Variables *****

    @Autowired
    private final SimplePushRepo simplePushRepo;

    // ***** Constructors *****

    public SimplePushController(SimplePushRepo simplePushRepo)
    {
        this.simplePushRepo = simplePushRepo;
    }

    // ***** Functions *****

    @GetMapping("/employee")
    public List<SimplePushModel> getAllEmployees()
    {
        return (List<SimplePushModel>) this.simplePushRepo.findAll();
    }

    @PostMapping("/create")
    public int createItem(@RequestBody SimplePushModel item){
        this.simplePushRepo.save(item);
        return 0;
    }
}
