package com.example.simplepcf.simplepush.Model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class SimplePushModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private double salary;

}



//    public List<GradesModel> getGrades()
//    {
//        return grades;
//    }
//
//    public void setGrades(List<GradesModel> grades)
//    {
//        this.grades = grades;
//    }
//
