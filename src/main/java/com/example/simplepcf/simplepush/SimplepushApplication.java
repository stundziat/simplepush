package com.example.simplepcf.simplepush;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimplepushApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(SimplepushApplication.class, args);
    }

}
