package com.example.simplepcf.simplepush.Repository;

import com.example.simplepcf.simplepush.Model.SimplePushModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SimplePushRepo extends CrudRepository<SimplePushModel, Long>
{
}
